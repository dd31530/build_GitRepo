#!/bin/ksh

set +x

# -----------------------------------------------------
# build_GitRepo.sh (DD | 151128)
# -----------------------------------------------------
# 
# résumé: initialisation d'un repo Git local et clonage sur GitLab
# prérequis:
#	-être propriétaire d'un compte <REMOTE_USER> sur GitLab (https://gitlab.com)
#	-avoir généré un couple de clefs ssh (privée, publique) en local
#	-avoir déposé la clef publique sur GitLab
#


# -----------------------------------------------------
# fonctions
# -----------------------------------------------------

function Usage
{
  echo "utilisation: $(basename $0) <nom de projet>"
  echo "	le paramètre <nom de projet> est obligatoire!"
  exit 1
}

function CheckSshAgent
{
echo "---------------------------"
echo "function CheckSshAgent"
echo "---------------------------"
# vérifie si un agent ssh tourne...
ps -ef | grep -v "grep" | grep "ssh-agent -s"
if [ $? -eq 1 ]
then
# dans la négative on le lance...
  eval "$(ssh-agent -s)"
  ssh-add ~$LOCAL_USER/.ssh/id_rsa
fi
}

function CheckGitLab
{
echo "---------------------------"
echo "function CheckGitLab"
echo "---------------------------"
# test de connexion à GitLab...
ssh -T git@$REMOTE_SERVER
}

function BuildLocalRepo
{
echo "---------------------------"
echo "function BuildLocalRepo"
echo "---------------------------"
# création du repo local...
if [ ! -d $LOCAL_WD/$REPO ]
then
  echo "création du repo local $REPO..."
  mkdir $LOCAL_WD/$REPO
  cd $LOCAL_WD/$REPO
  cat > $LOCAL_WD/$REPO/README.md << !EOF
# Projet: $REPO
# Date création: $(date)
# par: $LOCAL_USER
# informations système local:
# $LOCAL_SYSTEM
# version GIT local: $(git --version | awk '{print $3}')
!EOF
fi
}

function BuildRemoteRepo
{
echo "---------------------------"
echo "function BuildRemoteRepo"
echo "---------------------------"
# création du repo distant sur GitLab...
URL="https://$REMOTE_SERVER/api/v3/projects"
CURLCMD="curl --header \"PRIVATE-TOKEN:$TOKEN\" -X POST \"$URL?name=$REPO\""
eval $CURLCMD 2>&1 > /dev/null
}

function FirstCommit
{
echo "---------------------------"
echo "function FirstCommit"
echo "---------------------------"
# premier commit...
MESSAGE="initialisation $REPO; ajout README.md"
echo $MESSAGE
cd $LOCAL_WD/$REPO
git init
git add README.md
git commit -m "$MESSAGE"
git remote add origin git@$REMOTE_SERVER:$REMOTE_USER/${REPO}.git
git add .
git commit
git push -u origin master
}


# -----------------------------------------------------
# variables
# -----------------------------------------------------

LOCAL_USER=$(whoami)
LOCAL_WD=$(pwd)
LOCAL_SYSTEM=$(uname -a)

REMOTE_SERVER=gitlab.com
REMOTE_USER=${GITLAB_USER:-non positionné !}
TOKEN=${GITLAB_TOKEN:-non positionné !}


# -----------------------------------------------------
# main
# -----------------------------------------------------

[ $# -eq 0 ] && Usage
REPO=$1
[ "$REMOTE_USER" == "non positionné !" ] && echo "GITLAB_USER non positionné !" && exit 1
[ "$TOKEN" == "non positionné !" ] && echo "GITLAB_TOKEN non positionné !" && exit 1
CheckSshAgent
CheckGitLab
BuildLocalRepo
BuildRemoteRepo
FirstCommit

# eof
